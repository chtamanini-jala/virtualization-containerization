#!/bin/bash

# creating attachable network
docker network create --attachable mynetwork

# creating volumes for sonarqube
docker volume create --name sonarqube_data
docker volume create --name sonarqube_logs
docker volume create --name sonarqube_extensions

# run + volume
docker run -d --name my-sonarqube -e SONAR_JDBC_URL=jdbc:postgreqsql://host/sonardb -e SONAR_JDBC_USERNAME=christ -e SONAR_JDBC_PASSWORD=123456 -v sonarqube_data:/opt/sonarqube/data -v sonarqube_extensions:/opt/sonarqube/extensions -v sonarqube_logs:/opt/sonarqube/logs sonarqube:latest
docker run -d --name my-postgres -v /var/postgress_data -e POSTGRES_USER=christ -e POSTGRES_PASSWORD=123456 -e POSTGRES_DB=sonardb -p 9000:9000 postgres:latest
docker run -d --name my-jenkins -v /var/jenkins_data -p 8080:8080 jenkins:latest
docker run -d --name my-sonatype -v /var/sonatype_data -p 8081:8081 sonatype/nexus3:latest
docker run -d --name my-portainer -v /var/portainer_data -p 8082:8082 portainer/portainer:latest

#connect networks
docker network connect mynetwork my-jenkins
docker network connect mynetwork my-sonatype
docker network connect mynetwork my-portainer
docker network connect mynetwork my-sonarqube
docker network connect mynetwork my-postgres