<h3>Build and run your app with Compose</h3>

From your project directory, start up your application by running <strong>docker-compose up</strong>

Enter <strong>http://localhost:5000/ </strong>in a browser to see the application running.
