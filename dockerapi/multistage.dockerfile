# syntax=docker/Dockerfile:1

#FROM python:3.7-alpine AS build
#WORKDIR /code
#ENV FLASK_APP=app.py
#ENV FLASK_RUN_HOST=0.0.0.0
#RUN apk add --no-cache gcc musl-dev linux-headers
#COPY requirements.txt requirements.txt
#COPY . .
#RUN pip install -r requirements.txt


#FROM python:3.7-alpine
#WORKDIR /code
#COPY --from=build /code /code
#EXPOSE 5000
#VOLUME ["/code"]
#CMD ["flask", "run", "--port=5000"]

FROM python:3.7-alpine AS build
WORKDIR /code
RUN apk add --no-cache gcc musl-dev linux-headers
COPY requirements.txt requirements.txt

FROM python:3.7-alpine AS build2
COPY --from=build /code /code
WORKDIR /code
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0
RUN pip install -r requirements.txt jsonify
EXPOSE 5000
COPY . .
VOLUME ["/code"]
CMD ["flask", "run", "--port=5000"]